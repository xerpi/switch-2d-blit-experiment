#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdarg.h>
#include <switch.h>
#include <deko3d.h>

void __attribute__((format(printf, 1, 2))) LOG(const char *fmt, ...)
{
	char buf[256];
	va_list argptr;
	int n;

	va_start(argptr, fmt);
	n = vsnprintf(buf, sizeof(buf), fmt, argptr);
	va_end(argptr);

	svcOutputDebugString(buf, n);
}

void NORETURN fatal_error(const char *dialog_message, const char *fullscreen_message)
{
	extern u32 __nx_applet_exit_mode;
	ErrorApplicationConfig c;

	errorApplicationCreate(&c, dialog_message, fullscreen_message);
	errorApplicationShow(&c);

	__nx_applet_exit_mode = 1;
	exit(1);
}

void NORETURN __assert_func(const char *file, int line, const char *func, const char *failedexpr)
{
	char message[256];

	snprintf(message, sizeof(message), "assertion \"%s\" failed: file \"%s\", line %d%s%s\n",
		failedexpr, file, line, func ? ", function: " : "", func ? func : "");

	LOG(message);
	fatal_error("Assertion failed.", message);
}

static void dk_debug_callback(void *userData, const char *context, DkResult result, const char *message)
{
	char description[256];

	if (result == DkResult_Success) {
		LOG("deko3d debug callback: context: %s, message: %s, result %d",
		    context, message, result);
	} else {
		snprintf(description, sizeof(description), "context: %s, message: %s, result %d",
		         context, message, result);
		fatal_error("deko3d fatal error.", description);
	}
}

#define WIDTH 960
#define HEIGHT 544
#define STRIDE 1024

#define SWAPCHAIN_SIZE	2
#define CMDBUF_SIZE	4 * 1024

static DkDevice g_dk_device;
static DkQueue g_transfer_queue;
static DkMemBlock g_cmdbuf_memblock;
static DkCmdBuf g_cmdbuf;
static DkMemBlock g_orig_fb_memblock;
static DkMemBlock g_framebuffer_memblock;
static DkImage g_swapchain_images[SWAPCHAIN_SIZE];
static DkSwapchain g_swapchain;
static bool g_swapchain_created = false;
static uint32_t g_swapchain_image_width;
static uint32_t g_swapchain_image_height;
static DkFence g_acquire_fence;
static Thread g_presenter_thread;
static LEvent g_presenter_thread_run;
static void *g_base;
static uint32_t g_width, g_height, g_pitch;
static CondVar g_vblank_condvar;
static Mutex g_vblank_mutex;

static void create_swapchain(uint32_t width, uint32_t height)
{
	DkImageLayoutMaker image_layout_maker;
	DkImageLayout framebuffer_layout;
	DkMemBlockMaker memblock_maker;
	DkSwapchainMaker swapchain_maker;
	DkImage const *swapchain_images_ptrs[SWAPCHAIN_SIZE];
	uint32_t framebuffer_size, framebuffer_align;

	/* Calculate layout for the framebuffers */
	dkImageLayoutMakerDefaults(&image_layout_maker, g_dk_device);
	image_layout_maker.flags = DkImageFlags_Usage2DEngine | DkImageFlags_UsagePresent |
				   DkImageFlags_HwCompression;
	image_layout_maker.format = DkImageFormat_RGBA8_Unorm;
	image_layout_maker.dimensions[0] = width;
	image_layout_maker.dimensions[1] = height;
	dkImageLayoutInitialize(&framebuffer_layout, &image_layout_maker);

	/* Retrieve necessary size and alignment for the framebuffers */
	framebuffer_size  = dkImageLayoutGetSize(&framebuffer_layout);
	framebuffer_align = dkImageLayoutGetAlignment(&framebuffer_layout);
	framebuffer_size  = (framebuffer_size + framebuffer_align - 1) & ~(framebuffer_align - 1);

	/* Create a memory block that will host the framebuffers */
	dkMemBlockMakerDefaults(&memblock_maker, g_dk_device, SWAPCHAIN_SIZE * framebuffer_size);
	memblock_maker.flags = DkMemBlockFlags_GpuCached | DkMemBlockFlags_Image;
	g_framebuffer_memblock = dkMemBlockCreate(&memblock_maker);

	/* Initialize the framebuffers with the layout and backing memory we've just created */
	for (uint32_t i = 0; i < SWAPCHAIN_SIZE; i++) {
		swapchain_images_ptrs[i] = &g_swapchain_images[i];
		dkImageInitialize(&g_swapchain_images[i], &framebuffer_layout,
				  g_framebuffer_memblock, i * framebuffer_size);
	}

	/* Create a swapchain out of the framebuffers we've just initialized */
	dkSwapchainMakerDefaults(&swapchain_maker, g_dk_device, nwindowGetDefault(),
				 swapchain_images_ptrs, SWAPCHAIN_SIZE);
	g_swapchain = dkSwapchainCreate(&swapchain_maker);

	g_swapchain_image_width = width;
	g_swapchain_image_height = height;
	g_swapchain_created = true;
}

static void cmdbuf_copy_image(DkCmdBuf cmdbuf,
			      DkImage const *src_image, uint32_t src_width, uint32_t src_height,
			      DkImage const *dst_image, uint32_t dst_width, uint32_t dst_height)
{
	DkImageView src_view, dst_view;
	DkImageRect src_rect, dst_rect;

	src_rect.x = src_rect.y = src_rect.z = 0;
	src_rect.width = src_width;
	src_rect.height = src_height;
	src_rect.depth = 1;

	dst_rect.x = dst_rect.y = dst_rect.z = 0;
	dst_rect.width = dst_width;
	dst_rect.height = dst_height;
	dst_rect.depth = 1;

	dkImageViewDefaults(&src_view, src_image);
	dkImageViewDefaults(&dst_view, dst_image);
	dkCmdBufBlitImage(cmdbuf, &src_view, &src_rect, &dst_view, &dst_rect, 0, 1);
}

static void dkimage_for_existing_framebuffer(DkImage *image, const void *addr,
					     uint32_t width, uint32_t height, uint32_t stride)
{
	DkMemBlock memblock;
	DkImageLayoutMaker image_layout_maker;
	DkImageLayout image_layout;
	uint32_t offset;

	memblock = g_orig_fb_memblock;
	assert(memblock);

	dkImageLayoutMakerDefaults(&image_layout_maker, g_dk_device);
	image_layout_maker.flags = DkImageFlags_PitchLinear | DkImageFlags_Usage2DEngine;
	image_layout_maker.format = DkImageFormat_RGBA8_Unorm;
	image_layout_maker.dimensions[0] = width;
	image_layout_maker.dimensions[1] = height;
	image_layout_maker.pitchStride = stride * 4;
	dkImageLayoutInitialize(&image_layout, &image_layout_maker);

	offset = (uintptr_t)addr - (uintptr_t)dkMemBlockGetCpuAddr(memblock);
	dkImageInitialize(image, &image_layout, memblock, offset);
}

static void presenter_thread_func(void *arg)
{
	DkCmdList cmdlist;
	DkImage src_image;
	const void *base;
	uint32_t width, height, stride;
	int slot;
	DkFence present_fence;

     LOG("Thread!");

	while (leventTryWait(&g_presenter_thread_run)) {
		/* Make sure Vita has configured a framebuffer */
		if (!g_base) {
			svcSleepThread(16666667ull);
			continue;
		}

		base = g_base;
		width = g_width;
		height = g_height;
		stride = g_pitch;

		/* Reconfigure if sizes change and create the swapchain for the first time */
		if (!g_swapchain_created) {
            LOG("Create swapchain");
			create_swapchain(width, height);
		} else if (g_swapchain_image_width != width || g_swapchain_image_height != height) {
			dkSwapchainDestroy(g_swapchain);
			dkMemBlockDestroy(g_framebuffer_memblock);
			create_swapchain(width, height);
		}

		/* Build a DkImage for the source PSVita-configured framebuffer */
		dkimage_for_existing_framebuffer(&src_image, base, width, height, stride);

		/* Acquire a framebuffer from the swapchain */
		dkSwapchainAcquireImage(g_swapchain, &slot, &g_acquire_fence);

		/* Wait for the acquire fence to be signaled before starting the 2D transfer */
		dkCmdBufWaitFence(g_cmdbuf, &g_acquire_fence);

		/* 2D copy command from the PSVita-configured framebuffer to the swapchain framebuffer */
		cmdbuf_copy_image(g_cmdbuf, &src_image, width, height,
				  &g_swapchain_images[slot], width, height);

        /* Signal fence once the 2D transfer finishes */
        dkCmdBufSignalFence(g_cmdbuf, &present_fence, true);

		/* Finish the command list */
		cmdlist = dkCmdBufFinishList(g_cmdbuf);

        /* Submit the 2D copy command */
        dkQueueSubmitCommands(g_transfer_queue, cmdlist);

		/* Kick the transfer queue to start processing commands */
		dkQueueFlush(g_transfer_queue);

		/* Present the new frame, passing the transfer-finished fence */
		dkSwapchainPresentImage(g_swapchain, slot, &present_fence);

        dkFenceWait(&g_acquire_fence, -1);

        condvarWakeAll(&g_vblank_condvar);

        // LOG("Wake!");

		/* Wait until the transfer has finished */
		dkQueueWaitIdle(g_transfer_queue);

		/* The transfer has finished and the queue is idle, we can reset the command buffer */
		dkCmdBufClear(g_cmdbuf);
	}

	threadExit();
}

void draw_rectangle(void *base, uint32_t x, uint32_t y, uint32_t w, uint32_t h, uint32_t color)
{
    for (int i = 0; i < w; i++) {
        for (int j = 0; j < h; j++) {
            *(uint32_t *)((char *)base + ((x + j) + (y + i) * STRIDE) * 4) = color;
        }
    }
}

int main(int argc, char* argv[])
{
	DkDeviceMaker device_maker;
	DkQueueMaker queue_maker;
	DkMemBlockMaker memblock_maker;
	DkCmdBufMaker cmdbuf_maker;
	Result res;

	/* Initialize deko3d device */
	dkDeviceMakerDefaults(&device_maker);
	device_maker.userData = NULL;
	device_maker.cbDebug = dk_debug_callback;
	g_dk_device = dkDeviceCreate(&device_maker);

	/* Create graphics queue for transfers */
	dkQueueMakerDefaults(&queue_maker, g_dk_device);
	queue_maker.flags = DkQueueFlags_HighPrio;
	queue_maker.perWarpScratchMemorySize = 0;
	queue_maker.maxConcurrentComputeJobs = 0;
	g_transfer_queue = dkQueueCreate(&queue_maker);

	/* Create a memory block which will be used for recording command lists using a command buffer */
	dkMemBlockMakerDefaults(&memblock_maker, g_dk_device, CMDBUF_SIZE);
	memblock_maker.flags = DkMemBlockFlags_CpuUncached | DkMemBlockFlags_GpuCached;
	g_cmdbuf_memblock = dkMemBlockCreate(&memblock_maker);
    assert(g_cmdbuf_memblock);

	/* Create a command buffer object */
	dkCmdBufMakerDefaults(&cmdbuf_maker, g_dk_device);
	g_cmdbuf = dkCmdBufCreate(&cmdbuf_maker);
    assert(g_cmdbuf);

	/* Feed our memory to the command buffer so that we can start recording commands */
	dkCmdBufAddMemory(g_cmdbuf, g_cmdbuf_memblock, 0, CMDBUF_SIZE);

   // Calculate layout for the framebuffers
    DkImageLayoutMaker layout_maker;
    DkImageLayout layout;
    dkImageLayoutMakerDefaults(&layout_maker, g_dk_device);
	layout_maker.flags = DkImageFlags_PitchLinear | DkImageFlags_UsageRender | DkImageFlags_Usage2DEngine;
	layout_maker.format = DkImageFormat_RGBA8_Unorm;
	layout_maker.dimensions[0] = WIDTH;
	layout_maker.dimensions[1] = HEIGHT;
	layout_maker.pitchStride = STRIDE * 4;
    dkImageLayoutInitialize(&layout, &layout_maker);

    // Retrieve necessary size and alignment for the framebuffers
    uint32_t framebufferSize  = dkImageLayoutGetSize(&layout);
    uint32_t framebufferAlign = dkImageLayoutGetAlignment(&layout);
    framebufferSize = (framebufferSize + framebufferAlign - 1) & ~(framebufferAlign - 1);

    // Create a memory block that will host the framebuffers
    dkMemBlockMakerDefaults(&memblock_maker, g_dk_device, 2 * framebufferSize);
    memblock_maker.flags = DkMemBlockFlags_CpuUncached | DkMemBlockFlags_GpuCached | DkMemBlockFlags_Image;
    g_orig_fb_memblock = dkMemBlockCreate(&memblock_maker);
    assert(g_orig_fb_memblock);

    char *base = dkMemBlockGetCpuAddr(g_orig_fb_memblock);
    g_base = base;
    assert(base);
    g_width = WIDTH;
    g_height = HEIGHT;
    g_pitch = STRIDE;

	leventInit(&g_presenter_thread_run, true, false);

    LOG("starting thread...");

    mutexInit(&g_vblank_mutex);
    condvarInit(&g_vblank_condvar);

	res = threadCreate(&g_presenter_thread, presenter_thread_func, NULL, NULL, 0x10000, 28, -2);
	if (R_FAILED(res)) {
		LOG("Error creating VSync thread: 0x%lx", res);
		return res;
	}

	res = threadStart(&g_presenter_thread);
	if (R_FAILED(res)) {
		LOG("Error starting VSync thread: 0x%lx", res);
		return res;
	}


	// Main loop
    static uint32_t x = 0;
    static uint32_t frame = 1;
	while (appletMainLoop()) {
        void *cur_base = base + (frame * framebufferSize);

        for (int i = 0; i < HEIGHT; i++) {
            for (int j = 0; j < WIDTH; j++) {
                *(uint32_t *)((char *)cur_base + (j + i * STRIDE) * 4) = 0xFF000000 | ((x >> 1) & 0xFF);
            }
        }
        draw_rectangle(cur_base, 100, 100, 200, 100, 0xFFFF00FF);
        draw_rectangle(cur_base, 300, 300, 100, 200, 0xFF00FF00);
        x++;

        //dkFenceWait(&g_acquire_fence, -1);
        condvarWait(&g_vblank_condvar, &g_vblank_mutex);
        // LOG("VBlank");
        g_base = base;
        frame ^= 1;
	}

	leventClear(&g_presenter_thread_run);

	res = threadWaitForExit(&g_presenter_thread);
	if (R_FAILED(res)) {
		LOG("Error waiting for the presenter thread to finish: 0x%lx", res);
		return res;
	}

	return 0;
}
